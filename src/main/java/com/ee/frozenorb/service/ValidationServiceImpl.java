package com.ee.frozenorb.service;

import com.ee.frozenorb.dto.UserDTO;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

@Service
public class ValidationServiceImpl implements ValidationService{

    private Validator validator;

    ValidationServiceImpl(Validator validator) {
        this.validator = validator;
    }


    @Override
    public void validate(UserDTO user) {
        Set<ConstraintViolation<UserDTO>> violations = validator.validate(user);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
