package com.ee.frozenorb.util;

import com.ee.frozenorb.entity.User;
import com.ee.frozenorb.repository.UserRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserUnlockTask {

    private UserRepository userRepository;

    public UserUnlockTask(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Scheduled(fixedDelay = 60000)
    public void unlockUsers() {
        System.out.println("UNLOCKING USERS");
        List<User> users = userRepository.getUsersByIsLockedAndLockExpirationTimeLessThan(true, System.currentTimeMillis());
        users.stream().forEach(user ->
        {
                user.setLocked(false);
                user.setRemainingVerificationAttempts(3);
        });

        userRepository.saveAll(users);
    }
}
