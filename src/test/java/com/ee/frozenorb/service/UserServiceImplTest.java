package com.ee.frozenorb.service;

import com.ee.frozenorb.entity.User;
import com.ee.frozenorb.exceptionhandling.UserLockedException;
import com.ee.frozenorb.exceptionhandling.UserNotFoundException;
import com.ee.frozenorb.exceptionhandling.WrongVerificationCodeException;
import com.ee.frozenorb.repository.UserRepository;
import com.ee.frozenorb.repository.VerificationHistoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private SMSService smsService;

    @Mock
    private VerificationHistoryRepository verificationHistoryRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Captor
    private ArgumentCaptor<User> userArgumentCaptor;

    @Test
    public void saveUser() {
        var user = new User();
        user.setId(1L);

        userService.save(user);

        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void getUserByEmail() {
        var email = "asd@abv.bg";
        var user = new User();
        user.setEmail(email);
        Optional<User> optionalUser = Optional.of(user);

        when(userRepository.getByEmail(email)).thenReturn(optionalUser);

        Optional<User> optionalReturnedUser = userService.getUser(email);

        verify(userRepository, times(1)).getByEmail(email);

        assertEquals(optionalUser, optionalReturnedUser);
    }

    @Test
    public void returnTrueIfUserIsVerified() {
        var email = "asd@abv.bg";
        var user = new User();
        user.setEmail(email);
        user.setVerified(true);
        Optional<User> optionalUser = Optional.of(user);

        when(userRepository.getByEmail(email)).thenReturn(optionalUser);

        verify(smsService, never()).sendSms(any(), any());

        boolean isVerified = userService.isVerified(email);

        assertTrue(isVerified);
    }


    @Test
    public void verificationCodeIsSentToUserIfUserIsNotVerified() {
        var email = "asd@abv.bg";
        var phone = "359883445566";
        var user = new User();
        user.setEmail(email);
        user.setVerified(false);
        user.setPhone(phone);
        Optional<User> optionalUser = Optional.of(user);

        when(userRepository.getByEmail(email)).thenReturn(optionalUser);
        when(smsService.generateVerificationCode()).thenReturn("123");

        boolean isVerified = userService.isVerified(email);

        verify(userRepository, times(1)).save(userArgumentCaptor.capture());

        verify(smsService, times(1)).sendSms(phone, "123");

        assertEquals("123", userArgumentCaptor.getValue().getVerificationCode());
        assertFalse(isVerified);
    }

    @Test
    public void throwsExceptionIfTheUserForVerificationDoesNotExist() {
        var email = "asd@abv.bg";
        Optional<User> optionalUser = Optional.empty();

        when(userRepository.getByEmail(email)).thenReturn(optionalUser);

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            userService.isVerified(email);
        });
    }

    @Test
    public void userVerifySuccessfully() {
        var email = "asd@abv.bg";
        var verificationCode = "123456";
        var user = new User();
        user.setEmail(email);
        user.setVerified(false);
        user.setVerificationCode(verificationCode);
        user.setRemainingVerificationAttempts(3);

        Optional<User> optionalUser = Optional.of(user);

        when(userRepository.getByEmail(email)).thenReturn(optionalUser);

        userService. verify(email, verificationCode);

        verify(userRepository, times(1)).save(userArgumentCaptor.capture());

        assertEquals(email, userArgumentCaptor.getValue().getEmail());
        assertTrue(userArgumentCaptor.getValue().isVerified());
    }

    @Test
    public void userFailToVerifyAndHisRemainingVerifyAttemptsDecrease() {
        var email = "asd@abv.bg";
        var verificationCode = "123456";
        var user = new User();
        user.setEmail(email);
        user.setVerified(false);
        user.setVerificationCode("1234567");
        user.setRemainingVerificationAttempts(3);

        Optional<User> optionalUser = Optional.of(user);

        when(userRepository.getByEmail(email)).thenReturn(optionalUser);


        Assertions.assertThrows(WrongVerificationCodeException.class, () -> {
            try {
                userService.verify(email, verificationCode);
            } finally {
                verify(userRepository, times(1)).save(userArgumentCaptor.capture());
                assertEquals(2, userArgumentCaptor.getValue().getRemainingVerificationAttempts());
            }
        });
    }

    @Test
    public void userFailToVerifyAndIsLockedIfRemainingVerificationAttemptsAreZero() {
        var email = "asd@abv.bg";
        var verificationCode = "123456";
        var user = new User();
        user.setEmail(email);
        user.setVerified(false);
        user.setVerificationCode("1234567");
        user.setRemainingVerificationAttempts(1);

        Optional<User> optionalUser = Optional.of(user);

        when(userRepository.getByEmail(email)).thenReturn(optionalUser);

        Assertions.assertThrows(UserLockedException.class, () -> {

            try{
                userService.verify(email, verificationCode);
            } finally {
                verify(userRepository, times(2)).save(userArgumentCaptor.capture());
                assertEquals(0, userArgumentCaptor.getValue().getRemainingVerificationAttempts());
            }
        });

    }
}