package com.ee.frozenorb.service;

import com.ee.frozenorb.dto.UserDTO;

public interface ValidationService {

    void validate(UserDTO user);
}
