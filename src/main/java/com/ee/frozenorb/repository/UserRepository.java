package com.ee.frozenorb.repository;

import com.ee.frozenorb.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> getByEmail(String email);

    List<User> getUsersByIsLockedAndLockExpirationTimeLessThan(boolean isLocked, long expirationTime);
}
