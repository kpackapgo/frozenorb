package com.ee.frozenorb.exceptionhandling;

public class UserLockedException extends RuntimeException {

    private int remainingLockTime;

    public UserLockedException(int remainingLockTime) {

        this.remainingLockTime = remainingLockTime;
    }

    @Override
    public String getMessage() {
        return "User locked due to fail verification. You can try login again after " + remainingLockTime + " seconds";
    }
}
