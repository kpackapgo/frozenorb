package com.ee.frozenorb.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(length = 100, unique = true)
    @Size(max = 100)
    @Email
    private String email;

    @NotNull
    @Size(min = 8, max = 15)
    private String password;

    @NotNull
    @Column(length = 15, unique = true)
    private String phone;

    private boolean isVerified;

    private boolean isLocked;

    private String verificationCode;

    private int remainingVerificationAttempts = 3;

    public long lockExpirationTime;
}
