package com.ee.frozenorb.repository;

import com.ee.frozenorb.entity.VerificationHistory;
import org.springframework.data.repository.CrudRepository;

public interface VerificationHistoryRepository extends CrudRepository<VerificationHistory, Long> {
}
