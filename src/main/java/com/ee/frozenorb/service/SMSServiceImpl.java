package com.ee.frozenorb.service;


import com.ee.frozenorb.util.VerificationCodeGenerator;
import org.springframework.stereotype.Service;

/**
 * Empty sms service just for the purpose of keeping the integrity of the task
 */
@Service
public class SMSServiceImpl implements SMSService{


    private VerificationCodeGenerator verificationCodeGenerator;

    public  SMSServiceImpl(VerificationCodeGenerator verificationCodeGenerator) {
        this.verificationCodeGenerator = verificationCodeGenerator;
    }

    /**
     * This needs to contain a logic with an sms gateway and send the message to the user
     * @param phoneNumber the phone to which a message will be send
     * @param text the message
     */
    @Override
    public void sendSms(String phoneNumber, String text) {
    }

    public String generateVerificationCode() {
        return verificationCodeGenerator.generateCode(6);
    }
}
