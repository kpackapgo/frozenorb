package com.ee.frozenorb.service;

import com.ee.frozenorb.entity.User;
import com.ee.frozenorb.entity.VerificationHistory;
import com.ee.frozenorb.exceptionhandling.UserLockedException;
import com.ee.frozenorb.exceptionhandling.UserNotFoundException;
import com.ee.frozenorb.exceptionhandling.WrongVerificationCodeException;
import com.ee.frozenorb.repository.UserRepository;
import com.ee.frozenorb.repository.VerificationHistoryRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private VerificationHistoryRepository verificationHistoryRepository;
    private SMSService smsService;

    public UserServiceImpl(UserRepository userRepository, VerificationHistoryRepository verificationHistoryRepository, SMSService smsService) {
        this.userRepository = userRepository;
        this.verificationHistoryRepository = verificationHistoryRepository;
        this.smsService = smsService;
    }

    /**
     * update an existing user or save a new one in the database
     *
     * @param user that will be saved/updated
     */
    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    /**
     * Get a user by email address
     *
     * @param email the email of the user which we want to get
     * @return optional of a User object
     */
    @Override
    public Optional<User> getUser(String email) {
        return userRepository.getByEmail(email);
    }

    /**
     * Check if a user is verified or not
     *
     * @param email the email of the user
     * @return true if user is verified or false if not verified
     */
    @Override
    public Boolean isVerified(String email) {
        Optional<User> optionalUser = userRepository.getByEmail(email);
        if (optionalUser.isEmpty())  {
            throw new UserNotFoundException(email);
        }

        User user = optionalUser.get();
        if(user.isLocked()) {
            throw new UserLockedException(60);
        }

        if(!user.isVerified()) {
            String verificationCode = smsService.generateVerificationCode();
            smsService.sendSms(user.getPhone(), verificationCode);
            user.setVerificationCode(verificationCode);
            userRepository.save(user);
        }

        return user.isVerified();
    }

    /**
     * Verify an user matching a code that he provides with an existing one stored in the database
     *
     * @param email            the email of the user
     * @param verificationCode the verification code provided by the user
     */
    @Override
    public void verify(String email, String verificationCode) {
        User user = userRepository.getByEmail(email).get();
        int verificationAttemptsRemaining = user.getRemainingVerificationAttempts();
        if (verificationAttemptsRemaining > 0) {

            logVerifyAttempt(verificationCode, user);

            if (user.getVerificationCode().equals(verificationCode)) {
                user.setVerified(true);
                userRepository.save(user);
                //Will not send a real message unless a real sms gateway is used.
                smsService.sendSms(user.getPhone(), "Welcome to SmsBump");
            } else {
                verificationAttemptsRemaining--;
                user.setRemainingVerificationAttempts(verificationAttemptsRemaining);
                userRepository.save(user);

                if (verificationAttemptsRemaining == 0) {
                    user.setLocked(true);
                    user.setLockExpirationTime(System.currentTimeMillis()+60000);
                    userRepository.save(user);

                    throw new UserLockedException(60);
                } else {
                    throw new WrongVerificationCodeException(verificationAttemptsRemaining);
                }
            }
        }
    }

    private void logVerifyAttempt(String verificationCode, User user) {
        VerificationHistory historyEntry = VerificationHistory.builder()
                .userId(user.getId())
                .requiredCode(user.getVerificationCode())
                .providedCode(verificationCode)
                .build();
        verificationHistoryRepository.save(historyEntry);
    }
}
