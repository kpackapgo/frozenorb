package com.ee.frozenorb.dto;

import com.ee.frozenorb.exceptionhandling.MobileNumber;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO {

    @NotNull
    @Size(max = 100, message = "Email must be shorter than 100 characters")
    @Email(message = "Email must be valid")
    private String email;

    @NotNull
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-zA-Z])(?!\\w*$).{8,15}",
            message = "Password must be between 8 and 15 symbols and contain at least one letter, number, and special symbol")
    private String password;

    @MobileNumber(message = "Phone must contain only numbers, starts with 359 and be 12 characters long")
    private String phone;
}
