# FrozenOrb
A registration and login with OTP verification

# How to run
### From IDE:
Use IntelliJ/Eclipse and run the FrozenorbApplication class


### From docker:

1. run maven steps -> **clean, install** - in order to build the jar file needed for the docker

2. execute this to build docker container: **docker build -t frozenorb-docker .**


3. execute this to start the docker container: **docker run -p 8080:8080 frozenorb-docker**

Note: if the 

WARNING: database console is not working with the docker container. I was not able to configure it

## Database access
After you start the application use this link to open the h2 database console:

http://localhost:8080/h2-console

