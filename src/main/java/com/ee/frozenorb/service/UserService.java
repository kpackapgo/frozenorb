package com.ee.frozenorb.service;

import com.ee.frozenorb.entity.User;

import java.util.Optional;

public interface UserService {

    void save(User user);

    Optional<User> getUser(String email);

    Boolean isVerified(String email);

    void verify(String email, String verificationCode);
}
