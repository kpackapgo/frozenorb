package com.ee.frozenorb.service;

public interface SMSService {

    void sendSms(String phoneNumber, String text);

    String generateVerificationCode();
}
