package com.ee.frozenorb.exceptionhandling;

public class WrongVerificationCodeException extends RuntimeException {
    private int remainingVerificationTries;

    public WrongVerificationCodeException(int remainingVerificationTries) {

        this.remainingVerificationTries = remainingVerificationTries;
    }

    @Override
    public String getMessage() {
        return "Wrong verification code. Your account will be temporary locked after " + remainingVerificationTries + " fail attempts";
    }
}
