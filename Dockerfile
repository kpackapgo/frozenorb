FROM adoptopenjdk/openjdk11:jre-11.0.6_10-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG JAR_FILE=target/frozenorb-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar", "-web -webAllowOthers -tcp -tcpAllowOthers -browser"]