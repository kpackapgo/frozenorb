package com.ee.frozenorb.exceptionhandling;

public class UserNotFoundException extends RuntimeException {
    private String email;

    public UserNotFoundException(String email) {

        this.email = email;
    }

    @Override
    public String getMessage() {
        return "User with email " + email + " does not exists.";
    }
}
