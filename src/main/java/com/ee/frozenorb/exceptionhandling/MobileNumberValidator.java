package com.ee.frozenorb.exceptionhandling;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MobileNumberValidator implements ConstraintValidator<MobileNumber, String> {

    @Override
    public void initialize(MobileNumber mobileNumber) {
    }

    /**
     * Validate that a phone number is correct.
     * @param phone
     * @param cxt
     * @return
     */
    @Override
    public boolean isValid(String phone, ConstraintValidatorContext cxt) {
        return phone != null && phone.matches("359[0-9]{9}");
    }
}
