package com.ee.frozenorb.controller;

import com.ee.frozenorb.service.ValidationService;
import com.ee.frozenorb.dto.UserDTO;
import com.ee.frozenorb.entity.User;
import com.ee.frozenorb.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
public class UserController {

    private UserService userService;

    private ValidationService validationService;

    private ModelMapper modelMapper;

    public UserController(UserService userService, ValidationService validationService, ModelMapper modelMapper) {
        this.userService = userService;
        this.validationService = validationService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("register")
    public void register(@RequestBody UserDTO userDTO) {
        validationService.validate(userDTO);

        User user = modelMapper.map(userDTO, User.class);
        userService.save(user);
    }

    @PostMapping("login")
    public String login(@RequestBody Map<String, String> payload) {
//        Optional<User> optionalUser = userService.getUser(email);
//        if(optionalUser.isEmpty()) {
//            return "user does not exists";
//        }
//        User user = optionalUser.get();
//        if (!user.getPassword().equals(password)) {
//            return "wrong password";
//        }
//        if(!user.isVerified()) {
//            return "need verification";
//        }
        return "user login token";
    }

    @GetMapping("isverified/{email}")
    public Boolean isVerified(@PathVariable String email) {
        return userService.isVerified(email);
    }

    @PostMapping("verify")
    public void verify(@RequestBody Map<String, String> payload) {
        String email = payload.get("email");
        String verificationCode = payload.get("verificationCode");
        userService.verify(email, verificationCode);
    }
}
