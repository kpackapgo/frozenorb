package com.ee.frozenorb.controller;

import com.ee.frozenorb.dto.UserDTO;
import com.ee.frozenorb.entity.User;
import com.ee.frozenorb.exceptionhandling.UserLockedException;
import com.ee.frozenorb.exceptionhandling.UserNotFoundException;
import com.ee.frozenorb.exceptionhandling.WrongVerificationCodeException;
import com.ee.frozenorb.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    private UserDTO userDTO;

    private final ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        userDTO = new UserDTO();
        userDTO.setEmail("asdasd@abv.bg");
        userDTO.setPassword("abcd1234!");
        userDTO.setPhone("359883439653");
    }

    @Test
    public void validUserIsRegistered() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(userDTO);

        mockMvc.perform(post("/register").content(jsonInString).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(userService, times(1)).save(userArgumentCaptor.capture());

        User userToBeSaved = userArgumentCaptor.getValue();
        assertEquals(userDTO.getEmail(), userToBeSaved.getEmail());
        assertEquals(userDTO.getPassword(), userToBeSaved.getPassword());
        assertEquals(userDTO.getPhone(), userToBeSaved.getPhone());
    }

    @Test
    public void userWithInvalidEmailIsNotSaved() throws Exception {
        userDTO.setEmail("asdasdabv.bg");

        validateUserIsNotSaved(userDTO);
    }

    @Test
    public void userWithShorterPasswordThanTheMinimumIsNotSaved() throws Exception {
        userDTO.setPassword("ab1!");

        validateUserIsNotSaved(userDTO);
    }

    @Test
    public void userWithLongerPasswordThanTheMaximumIsNotSaved() throws Exception {
        userDTO.setPassword("abcd1234567890abcd1!");

        validateUserIsNotSaved(userDTO);
    }


    @Test
    public void userWithPasswordWithoutLetterInItIsNotSaved() throws Exception {
        userDTO.setPassword("12345678!");

        validateUserIsNotSaved(userDTO);
    }

    @Test
    public void userWithPasswordWithoutNumberInItIsNotSaved() throws Exception {
        userDTO.setPassword("abcdefgh!");

        validateUserIsNotSaved(userDTO);
    }

    @Test
    public void userWithPasswordWithoutSpecialSymbolInItIsNotSaved() throws Exception {
        userDTO.setPassword("abcd1234");

        validateUserIsNotSaved(userDTO);
    }

    @Test
    public void userWithoutPhoneIsNotSaved() throws Exception {
        userDTO.setPhone(null);

        validateUserIsNotSaved(userDTO);
    }


    @Test
    public void userWithShorterPhoneNumberThanExpectedIsNotSaved() throws Exception {
        userDTO.setPhone("459883439623");

        validateUserIsNotSaved(userDTO);
    }

    @Test

    public void userWithLongerPhoneNumberThanExpectIsNotSaved() throws Exception {
        userDTO.setPhone("45988343962331231");

        validateUserIsNotSaved(userDTO);
    }

    @Test
    public void userWithPhoneWithWrongCountryCodyIsNotSaved() throws Exception {
        userDTO.setPhone("459883439623");

        validateUserIsNotSaved(userDTO);
    }

    private void validateUserIsNotSaved(UserDTO userDTO) throws Exception {

        String jsonInString = mapper.writeValueAsString(userDTO);

        mockMvc.perform(post("/register").content(jsonInString).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(userService, never()).save(any());
    }

    @Test
    public void userIsVerified() throws Exception {
        String email = "randomemail@abv.bg";

        when(userService.isVerified(email)).thenReturn(true);

        mockMvc.perform(get("/isverified/"+email))
                .andExpect(status().isOk()).andExpect(content().string("true"));
    }

    @Test
    public void userIsNotVerified() throws Exception {
        String email = "randomemail@abv.bg";

        when(userService.isVerified(email)).thenReturn(false);

        mockMvc.perform(get("/isverified/"+email))
                .andExpect(status().isOk()).andExpect(content().string("false"));
    }

    @Test
    public void throwsErrorIfUserThatIsBeingVerifiedDoesNotExists() throws Exception {
        String email = "randomemail@abv.bg";

        when(userService.isVerified(email)).thenThrow(new UserNotFoundException(email));

        mockMvc.perform(get("/isverified/"+email))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof UserNotFoundException));
    }

    @Test
    public void userSuccessfullyVerifyHisPhone() throws Exception {

        Map<String, String> payload = new HashMap<>();
        payload.put("email", "randomemail@gmail.com");
        payload.put("verificationCode", "123456");
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(payload);

        mockMvc.perform(post("/verify").content(jsonInString).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void userProvidesWrongVerificationAndExceptionIsThrown() throws Exception {

        Map<String, String> payload = new HashMap<>();
        payload.put("email", "randomemail@gmail.com");
        payload.put("verificationCode", "123456");
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(payload);

        doThrow(new WrongVerificationCodeException(2)).when(userService).verify("randomemail@gmail.com", "123456");

        mockMvc.perform(post("/verify").content(jsonInString).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof WrongVerificationCodeException));
    }

    @Test
    public void userProvidesWrongVerificationAndIsLockedWhenNoMoreAttempts() throws Exception {

        Map<String, String> payload = new HashMap<>();
        payload.put("email", "randomemail@gmail.com");
        payload.put("verificationCode", "123456");
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(payload);

        doThrow(new UserLockedException(60)).when(userService).verify("randomemail@gmail.com", "123456");

        mockMvc.perform(post("/verify").content(jsonInString).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof UserLockedException));
    }
}